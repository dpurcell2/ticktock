from django.forms import ModelForm
from django import forms

from timeclock.models import Record

class UpdateRecordForm(ModelForm):
    class Meta:
        model = Record
        fields = ['project', 'start_time', 'end_time', 'message']

class DateRangeForm(forms.Form):
    from_date = forms.DateField()
    to_date = forms.DateField()
