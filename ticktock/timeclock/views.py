from django.http import HttpResponse, HttpResponseRedirect
from django.shortcuts import render, get_object_or_404
from django.core.urlresolvers import reverse
from django.core.exceptions import ObjectDoesNotExist
from django.contrib.auth.decorators import login_required
from django.contrib.auth.models import User
from django.contrib.auth.decorators import login_required
from django.utils.decorators import method_decorator

from django.views.generic import ListView, FormView, UpdateView, CreateView, DeleteView, DetailView

from timeclock.models import Record, Project
from timeclock.forms import UpdateRecordForm, DateRangeForm

class PunchList(ListView):
    model = Record
    context_object_name = 'records'

    def get_queryset(self):
        records = Record.objects.filter(user = self.request.user).order_by('-start_time')
        if self.request.GET.items():
            date_range = DateRangeForm(self.request.GET)
            if date_range.is_valid():
                cd = date_range.cleaned_data
                records = records.filter(start_time__range=[cd.get('from_date'), cd.get('to_date')])
        return records

    def get_context_data(self, **kwargs):
        # Get the superclass to do its job
        context = super(PunchList, self).get_context_data(**kwargs)
        
        # Include the date range form
        context['date_range_form'] = DateRangeForm()
        # improve this by supplying an 'initla' to the DateRangeForm

        # Figure out the total time
        total_time = 0
        records = Record.objects.filter(user = self.request.user)
        
        # Did they use the date range in the form?
        if self.request.GET.items():
            date_range = DateRangeForm(self.request.GET)
            if date_range.is_valid():
                cd = date_range.cleaned_data
                records = records.filter(start_time__range=[cd.get('from_date'), cd.get('to_date')])

        for record in records:
            if record.end_time is not None: 
                total_time += (record.end_time - record.start_time).seconds

        hours, remainder = divmod(total_time, 3600)
        minutes, seconds = divmod(remainder, 60)
        total_time_string = "%02d:%02d:%02d" % (hours, minutes, seconds)
        context['total_time'] = total_time_string
        return context


    @method_decorator(login_required)
    def dispatch(self, *args, **kwargs):
        return super(PunchList, self).dispatch(*args, **kwargs)


class ClockIn(FormView):
    pass

class ClockOut(FormView):
    pass

class RecordUpdate(UpdateView):
    model = Record
    form_class = UpdateRecordForm

    def get_object(self):
        return get_object_or_404(Record, user = self.request.user, pk = self.kwargs['pk'])

    @method_decorator(login_required)
    def dispatch(self, *args, **kwargs):
        return super(RecordUpdate, self).dispatch(*args, **kwargs)

class RecordDelete(DeleteView):
    pass

class RecordAdd(CreateView):
    model = Record
    form_class = UpdateRecordForm

    def get_form(self, form_class):
        form = super(RecordAdd, self).get_form(form_class)
        form.instance.user = self.request.user
        return form


    @method_decorator(login_required)
    def dispatch(self, *args, **kwargs):
        return super(RecordAdd, self).dispatch(*args, **kwargs)

class RecordDetail(DetailView):
    model = Record
    context_object_name = 'record'

    def get_object(self):
        return get_object_or_404(Record, user = self.request.user, pk = self.kwargs['pk'])

    @method_decorator(login_required)
    def dispatch(self, *args, **kwargs):
        return super(RecordDetail, self).dispatch(*args, **kwargs)
