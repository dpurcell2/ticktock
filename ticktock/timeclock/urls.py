from django.conf.urls import patterns, url

from timeclock import views

urlpatterns = patterns('',
    url(r'^$', views.PunchList.as_view(), name='index'), # Last Few Punches
    url(r'^clock-in/$', views.ClockIn.as_view(), name='clock-in'),
    url(r'^clock-out/$', views.ClockOut.as_view(), name='clock-out'),
    url(r'^record/(?P<pk>\d+)/$', views.RecordDetail.as_view(), name='record-detail'),
    url(r'^record/(?P<pk>\d+)/edit/$', views.RecordUpdate.as_view(), name='record-update'),
    url(r'^record/(?P<pk>\d+)/delete/$', views.RecordDelete.as_view(), name='record-delete'),
    url(r'^record/add/$', views.RecordAdd.as_view(), name='record-add'),
)

